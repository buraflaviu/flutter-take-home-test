part of 'shop_bloc.dart';

@immutable
abstract class ShopEvent {}

class GetAllItems extends ShopEvent {}

class ProductSelected extends ShopEvent {}