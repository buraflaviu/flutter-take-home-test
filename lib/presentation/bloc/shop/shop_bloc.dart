import 'package:bloc/bloc.dart';
import 'package:flutter_take_home_test/domain/usecases/shop_usecase.dart';
import 'package:meta/meta.dart';

import '../../../domain/entities/item_entity.dart';

part 'shop_event.dart';

part 'shop_state.dart';

class ShopBloc extends Bloc<ShopEvent, ShopState> {
  ShopBloc(ShopUsecase useCase) : super(ShopInitial()) {
    on<GetAllItems>((event, emit) async {
      emit(ShopIsLoading());
      (await useCase.getItems()).fold(
          (failure) =>
              emit(ShopFetchFailed(failMessage: failure.failureMessage)),
          (response) => emit(ShopFetchSuccess(listOfItems: response)));
    });
  }
}
