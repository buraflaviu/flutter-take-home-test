part of 'shop_bloc.dart';

@immutable
abstract class ShopState {}

class ShopInitial extends ShopState {}

@immutable
class ShopIsLoading extends ShopState {}

@immutable
class ShopFetchFailed extends ShopState {
  final String failMessage;

  ShopFetchFailed({required this.failMessage});
}

@immutable
class ShopFetchSuccess extends ShopState {
  final List<Item> listOfItems;

  ShopFetchSuccess({required this.listOfItems});
}
