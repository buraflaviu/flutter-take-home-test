import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter_take_home_test/domain/usecases/shop_usecase.dart';
import 'package:meta/meta.dart';

part 'auth_event.dart';

part 'auth_state.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  AuthBloc(ShopUsecase useCase) : super(AuthInitialState()) {
    on<InitiateLogin>((event, emit) async {
      emit(LoginIsLoading());
      (await useCase.login(event.username, event.password)).fold(
          (failure) => emit(LoginFailed(failMessage: failure.failureMessage)),
          (response) => emit(LoginSuccess(userId: response)));
    });
  }
}
