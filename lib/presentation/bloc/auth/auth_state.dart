part of 'auth_bloc.dart';

@immutable
abstract class AuthState {}

@immutable
class AuthInitialState extends AuthState {}

@immutable
class LoginIsLoading extends AuthState {}

@immutable
class LoginFailed extends AuthState {
  final String failMessage;

  LoginFailed({required this.failMessage});
}

@immutable
class LoginSuccess extends AuthState {
  final String userId;

  LoginSuccess({required this.userId});
}