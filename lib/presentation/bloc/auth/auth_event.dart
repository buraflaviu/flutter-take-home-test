part of 'auth_bloc.dart';

@immutable
abstract class AuthEvent {}

@immutable
class InitiateLogin extends AuthEvent {
  var username;
  var password;

  InitiateLogin({required this.username, required this.password});
}