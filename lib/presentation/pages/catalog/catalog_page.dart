import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_take_home_test/data/repositories_impl/auth_repo_impl.dart';
import 'package:flutter_take_home_test/data/repositories_impl/shop_repo_impl.dart';
import 'package:flutter_take_home_test/domain/usecases/shop_usecase.dart';
import 'package:flutter_take_home_test/presentation/bloc/shop/shop_bloc.dart';

import '../../../core/util/const.dart';
import '../itemdetails/item_details_page.dart';

class CatalogPage extends StatefulWidget {
  const CatalogPage({Key? key}) : super(key: key);

  @override
  _CatalogPageState createState() => _CatalogPageState();
}

class _CatalogPageState extends State<CatalogPage> {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => ShopBloc(
          ShopUsecase(authRepo: AuthRepoImpl(), shopRepo: ShopRepoImpl()))
        ..add(GetAllItems()),
      child: Scaffold(
        backgroundColor: Colors.white,
        body: BlocBuilder<ShopBloc, ShopState>(
          builder: (context, state) {
            if (state is ShopFetchSuccess) {
              return ListView.separated(
                key: const Key('ListView'),
                shrinkWrap: true,
                itemCount: state.listOfItems.length + 1,
                separatorBuilder: kbuildSeparator,
                itemBuilder: (_, index) {
                  if (index == state.listOfItems.length + 1) {
                    return const SizedBox(height: 200);
                  }
                  return GestureDetector(
                      onTap: () {
                        //push the bloc to the details page for future functionalities (maybe press on icon or something, who knows)
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (_) => BlocProvider.value(
                                child: ItemDetailsPage(
                                    item: state.listOfItems[index]),
                                value: context.read<ShopBloc>())));
                      },
                      child: Column(
                        children: [
                          Text(state.listOfItems[index].name),
                          Image.network(state.listOfItems[index].url)
                        ],
                      ));
                },
              );
            } else if (state is ShopFetchFailed) {
              //return a toast here with error
            }
            return const Center(
              child: CircularProgressIndicator(),
            );
          },
        ),
      ),
    );
  }
}
