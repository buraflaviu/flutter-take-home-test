import 'package:flutter/material.dart';

import '../../../domain/entities/item_entity.dart';

class ItemDetailsPage extends StatelessWidget {
  final Item item;

  const ItemDetailsPage({Key? key, required this.item}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        children: [
          Text(item.name),
          Image.network(item.url),
        ],
      ),
    );
  }
}
