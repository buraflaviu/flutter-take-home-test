import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_take_home_test/data/repositories_impl/auth_repo_impl.dart';
import 'package:flutter_take_home_test/data/repositories_impl/shop_repo_impl.dart';
import 'package:flutter_take_home_test/domain/usecases/shop_usecase.dart';
import 'package:flutter_take_home_test/presentation/bloc/auth/auth_bloc.dart';

import '../../../core/util/const.dart';
import '../catalog/catalog_page.dart';
import '../shared_widgets/rounded_button.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  late String email;
  late String password;

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => AuthBloc(
          ShopUsecase(authRepo: AuthRepoImpl(), shopRepo: ShopRepoImpl())),
      child: Scaffold(
        backgroundColor: Colors.white,
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 24.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              const Flexible(
                child: Hero(
                  tag: 'logo',
                  child: SizedBox(
                    height: 200.0,
                    child: Text("Logo haha"),
                  ),
                ),
              ),
              const SizedBox(
                height: 48.0,
              ),
              TextField(
                keyboardType: TextInputType.emailAddress,
                textAlign: TextAlign.center,
                onChanged: (value) {
                  email = value;
                },
                decoration:
                    kTextFieldDecoration.copyWith(hintText: 'Enter your email'),
              ),
              const SizedBox(
                height: 8.0,
              ),
              TextField(
                obscureText: true,
                textAlign: TextAlign.center,
                onChanged: (value) {
                  password = value;
                },
                decoration: kTextFieldDecoration.copyWith(
                    hintText: 'Enter your password'),
              ),
              const SizedBox(
                height: 24.0,
              ),
              BlocConsumer<AuthBloc, AuthState>(listener: (context, state) {
                if (state is LoginSuccess) {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const CatalogPage()),
                  );
                }
              }, builder: (context, state) {
                if (state is LoginIsLoading || state is AuthInitialState) {
                  return const Center(
                    child: CircularProgressIndicator(),
                  );
                }
                if(state is LoginFailed){
                  //return a toast here with error
                }
                return RoundedButton(
                    title: 'Log In',
                    colour: Colors.lightBlueAccent,
                    onPressed: () {
                      context.read<AuthBloc>().add(
                          InitiateLogin(username: email, password: password));
                    });
              }),
            ],
          ),
        ),
      ),
    );
  }
}
