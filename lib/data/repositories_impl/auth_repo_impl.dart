import 'package:dartz/dartz.dart';
import 'package:flutter_take_home_test/data/datasource/remotedata/firebase_data.dart';
import 'package:flutter_take_home_test/domain/repositories_contracts/auth_repo.dart';

import '../../core/errors/failures.dart';

class AuthRepoImpl extends AuthRepo {
  @override
  Future<Either<Failure, String>> login(
      String username, String password) async {
    try {
      var remoteData = FirebaseRemoteAPI();
      return right(await remoteData.login(username, password));
    } on NoDataFetched {
      return const Left(ServerFailure(failureMessage: "Login fails"));
    }
  }
}
