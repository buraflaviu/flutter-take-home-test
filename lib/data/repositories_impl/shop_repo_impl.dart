import 'package:dartz/dartz.dart';
import 'package:flutter_take_home_test/data/datasource/remotedata/firebase_data.dart';
import 'package:flutter_take_home_test/domain/entities/item_entity.dart';

import '../../core/errors/failures.dart';
import '../../domain/repositories_contracts/shop_repo.dart';

class ShopRepoImpl extends ShopRepo {
  @override
  Future<Either<Failure, List<Item>>> getItems() async {
    try {
      var remoteData = FirebaseRemoteAPI();
      return right(await remoteData.getItems());
    } on NoDataFetched {
      return const Left(ServerFailure(failureMessage: "lol no items"));
    }
  }
}
