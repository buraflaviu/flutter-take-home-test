import 'dart:html';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_take_home_test/domain/entities/item_entity.dart';

class ItemModel extends Item {
  @override
  String name;
  @override
  double price;
  @override
  String url;

  ItemModel({required this.name, required this.price, required this.url})
      : super(name: name, price: price, url: url);

  factory ItemModel.fromSnapshot(QueryDocumentSnapshot snapshot) =>
      ItemModel(name: snapshot["name"], price: snapshot["price"], url: snapshot["url"]);

  Map<String, dynamic> toJson(ItemModel instance) => <String, dynamic>{
        'name': instance.name,
        'price': instance.price,
        'url': instance.url,
      };
}
