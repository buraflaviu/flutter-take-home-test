import 'package:flutter_take_home_test/data/model/items_model.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class FirebaseRemoteAPI {
  final _auth = FirebaseAuth.instance;
  final _fireStore = FirebaseFirestore.instance;

  Future<String> login(String username, String password) async {
    UserCredential user = await _auth.signInWithEmailAndPassword(
        email: username, password: password);
    return user.user!.uid;
  }

  Future<List<ItemModel>> getItems() async {
    List<ItemModel> listOfItems = [];
    await _fireStore
        .collection("items")
        .get()
        .then((QuerySnapshot querySnapshot) {
      for (var doc in querySnapshot.docs) {
        listOfItems.add(ItemModel.fromSnapshot(doc));
      }
    });
    return listOfItems;
  }
}
