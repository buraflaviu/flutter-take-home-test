import 'package:dartz/dartz.dart';
import 'package:flutter_take_home_test/domain/repositories_contracts/auth_repo.dart';
import 'package:flutter_take_home_test/domain/repositories_contracts/shop_repo.dart';

import '../../core/errors/failures.dart';
import '../entities/item_entity.dart';

class ShopUsecase {
   AuthRepo authRepo;

  ShopRepo shopRepo;

  ShopUsecase({required this.authRepo, required this.shopRepo});

  Future<Either<Failure, String>> login(
      String username, String password) async {
    return await authRepo.login(username, password);
  }

  Future<Either<Failure, List<Item>>> getItems() async {
    return shopRepo.getItems();
  }
}
