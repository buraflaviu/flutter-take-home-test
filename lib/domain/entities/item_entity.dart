class Item {
  String name;
  double price;

  //will be url in future
  String url;

  Item({required this.name, required this.price, required this.url});
}
