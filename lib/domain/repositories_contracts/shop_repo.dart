
import 'package:dartz/dartz.dart';

import '../../core/errors/failures.dart';
import '../entities/item_entity.dart';

abstract class ShopRepo {
  Future<Either<Failure, List<Item>>> getItems();
}