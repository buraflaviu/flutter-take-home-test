// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'failures.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$FailureTearOff {
  const _$FailureTearOff();

  NoDataFetched<T> noDataFetched<T>({required String failureMessage}) {
    return NoDataFetched<T>(
      failureMessage: failureMessage,
    );
  }

  ServerFailure<T> serverFailure<T>({required String failureMessage}) {
    return ServerFailure<T>(
      failureMessage: failureMessage,
    );
  }
}

/// @nodoc
const $Failure = _$FailureTearOff();

/// @nodoc
mixin _$Failure<T> {
  String get failureMessage => throw _privateConstructorUsedError;

  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String failureMessage) noDataFetched,
    required TResult Function(String failureMessage) serverFailure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String failureMessage)? noDataFetched,
    TResult Function(String failureMessage)? serverFailure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String failureMessage)? noDataFetched,
    TResult Function(String failureMessage)? serverFailure,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(NoDataFetched<T> value) noDataFetched,
    required TResult Function(ServerFailure<T> value) serverFailure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(NoDataFetched<T> value)? noDataFetched,
    TResult Function(ServerFailure<T> value)? serverFailure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(NoDataFetched<T> value)? noDataFetched,
    TResult Function(ServerFailure<T> value)? serverFailure,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $FailureCopyWith<T, Failure<T>> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FailureCopyWith<T, $Res> {
  factory $FailureCopyWith(Failure<T> value, $Res Function(Failure<T>) then) =
      _$FailureCopyWithImpl<T, $Res>;
  $Res call({String failureMessage});
}

/// @nodoc
class _$FailureCopyWithImpl<T, $Res> implements $FailureCopyWith<T, $Res> {
  _$FailureCopyWithImpl(this._value, this._then);

  final Failure<T> _value;
  // ignore: unused_field
  final $Res Function(Failure<T>) _then;

  @override
  $Res call({
    Object? failureMessage = freezed,
  }) {
    return _then(_value.copyWith(
      failureMessage: failureMessage == freezed
          ? _value.failureMessage
          : failureMessage // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
abstract class $NoDataFetchedCopyWith<T, $Res>
    implements $FailureCopyWith<T, $Res> {
  factory $NoDataFetchedCopyWith(
          NoDataFetched<T> value, $Res Function(NoDataFetched<T>) then) =
      _$NoDataFetchedCopyWithImpl<T, $Res>;
  @override
  $Res call({String failureMessage});
}

/// @nodoc
class _$NoDataFetchedCopyWithImpl<T, $Res>
    extends _$FailureCopyWithImpl<T, $Res>
    implements $NoDataFetchedCopyWith<T, $Res> {
  _$NoDataFetchedCopyWithImpl(
      NoDataFetched<T> _value, $Res Function(NoDataFetched<T>) _then)
      : super(_value, (v) => _then(v as NoDataFetched<T>));

  @override
  NoDataFetched<T> get _value => super._value as NoDataFetched<T>;

  @override
  $Res call({
    Object? failureMessage = freezed,
  }) {
    return _then(NoDataFetched<T>(
      failureMessage: failureMessage == freezed
          ? _value.failureMessage
          : failureMessage // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$NoDataFetched<T> implements NoDataFetched<T> {
  const _$NoDataFetched({required this.failureMessage});

  @override
  final String failureMessage;

  @override
  String toString() {
    return 'Failure<$T>.noDataFetched(failureMessage: $failureMessage)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is NoDataFetched<T> &&
            const DeepCollectionEquality()
                .equals(other.failureMessage, failureMessage));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(failureMessage));

  @JsonKey(ignore: true)
  @override
  $NoDataFetchedCopyWith<T, NoDataFetched<T>> get copyWith =>
      _$NoDataFetchedCopyWithImpl<T, NoDataFetched<T>>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String failureMessage) noDataFetched,
    required TResult Function(String failureMessage) serverFailure,
  }) {
    return noDataFetched(failureMessage);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String failureMessage)? noDataFetched,
    TResult Function(String failureMessage)? serverFailure,
  }) {
    return noDataFetched?.call(failureMessage);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String failureMessage)? noDataFetched,
    TResult Function(String failureMessage)? serverFailure,
    required TResult orElse(),
  }) {
    if (noDataFetched != null) {
      return noDataFetched(failureMessage);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(NoDataFetched<T> value) noDataFetched,
    required TResult Function(ServerFailure<T> value) serverFailure,
  }) {
    return noDataFetched(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(NoDataFetched<T> value)? noDataFetched,
    TResult Function(ServerFailure<T> value)? serverFailure,
  }) {
    return noDataFetched?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(NoDataFetched<T> value)? noDataFetched,
    TResult Function(ServerFailure<T> value)? serverFailure,
    required TResult orElse(),
  }) {
    if (noDataFetched != null) {
      return noDataFetched(this);
    }
    return orElse();
  }
}

abstract class NoDataFetched<T> implements Failure<T> {
  const factory NoDataFetched({required String failureMessage}) =
      _$NoDataFetched<T>;

  @override
  String get failureMessage;
  @override
  @JsonKey(ignore: true)
  $NoDataFetchedCopyWith<T, NoDataFetched<T>> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ServerFailureCopyWith<T, $Res>
    implements $FailureCopyWith<T, $Res> {
  factory $ServerFailureCopyWith(
          ServerFailure<T> value, $Res Function(ServerFailure<T>) then) =
      _$ServerFailureCopyWithImpl<T, $Res>;
  @override
  $Res call({String failureMessage});
}

/// @nodoc
class _$ServerFailureCopyWithImpl<T, $Res>
    extends _$FailureCopyWithImpl<T, $Res>
    implements $ServerFailureCopyWith<T, $Res> {
  _$ServerFailureCopyWithImpl(
      ServerFailure<T> _value, $Res Function(ServerFailure<T>) _then)
      : super(_value, (v) => _then(v as ServerFailure<T>));

  @override
  ServerFailure<T> get _value => super._value as ServerFailure<T>;

  @override
  $Res call({
    Object? failureMessage = freezed,
  }) {
    return _then(ServerFailure<T>(
      failureMessage: failureMessage == freezed
          ? _value.failureMessage
          : failureMessage // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$ServerFailure<T> implements ServerFailure<T> {
  const _$ServerFailure({required this.failureMessage});

  @override
  final String failureMessage;

  @override
  String toString() {
    return 'Failure<$T>.serverFailure(failureMessage: $failureMessage)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is ServerFailure<T> &&
            const DeepCollectionEquality()
                .equals(other.failureMessage, failureMessage));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(failureMessage));

  @JsonKey(ignore: true)
  @override
  $ServerFailureCopyWith<T, ServerFailure<T>> get copyWith =>
      _$ServerFailureCopyWithImpl<T, ServerFailure<T>>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String failureMessage) noDataFetched,
    required TResult Function(String failureMessage) serverFailure,
  }) {
    return serverFailure(failureMessage);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String failureMessage)? noDataFetched,
    TResult Function(String failureMessage)? serverFailure,
  }) {
    return serverFailure?.call(failureMessage);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String failureMessage)? noDataFetched,
    TResult Function(String failureMessage)? serverFailure,
    required TResult orElse(),
  }) {
    if (serverFailure != null) {
      return serverFailure(failureMessage);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(NoDataFetched<T> value) noDataFetched,
    required TResult Function(ServerFailure<T> value) serverFailure,
  }) {
    return serverFailure(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(NoDataFetched<T> value)? noDataFetched,
    TResult Function(ServerFailure<T> value)? serverFailure,
  }) {
    return serverFailure?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(NoDataFetched<T> value)? noDataFetched,
    TResult Function(ServerFailure<T> value)? serverFailure,
    required TResult orElse(),
  }) {
    if (serverFailure != null) {
      return serverFailure(this);
    }
    return orElse();
  }
}

abstract class ServerFailure<T> implements Failure<T> {
  const factory ServerFailure({required String failureMessage}) =
      _$ServerFailure<T>;

  @override
  String get failureMessage;
  @override
  @JsonKey(ignore: true)
  $ServerFailureCopyWith<T, ServerFailure<T>> get copyWith =>
      throw _privateConstructorUsedError;
}
