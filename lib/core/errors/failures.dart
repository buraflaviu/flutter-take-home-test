import 'package:freezed_annotation/freezed_annotation.dart';

part 'failures.freezed.dart';

@freezed
class Failure<T> with _$Failure<T> {
  const factory Failure.noDataFetched({required String failureMessage}) =
  NoDataFetched<T>;
  const factory Failure.serverFailure({required String failureMessage}) =
  ServerFailure<T>;

}