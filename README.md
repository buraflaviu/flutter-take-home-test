# Mason

***Mustache documentation:***  [https://mustache.github.io/mustache.5.html](https://mustache.github.io/mustache.5.html)

**Client side :**

- Install Mason CLI. ([https://pub.dev/packages/mason_cli](https://pub.dev/packages/mason_cli))
- mason init
- In mason.yaml you will need to configure the path and location of the bricks  
```jsx
//Register bricks which can be consumed via the Mason CLI.
bricks:  
    core_configuration:  
        git:  
          url: https://dev.azure.com/ht2s/Flutter%20Team/_git/Flutter%20Core
          path: bricks/core_configuration  
```
- mason get ( to get all the bricks that you registered)
- use the brick declared in .yaml file

```jsx
//to generate/activate a specific brick
mason make <brick_name>
```

**Server side:**

You only need to create a brick folder where your brick will be located.

```jsx
//to generate/activate a specific brick
mason new <brick_name>
```

Recommendation: *install mason to test your bricks before push*


**Other useful commands:**
<pre>
mason
🧱  mason • lay the foundation!  

Usage: mason <command> [arguments]  

Global options:
-h, --help       Print this usage information.  
--version    Print the current version.  

Available commands:  
add        Adds a brick from a local or remote source.  
bundle     Generates a bundle from a brick template.  
cache      Interact with mason cache.  
get        Gets all bricks in the nearest mason.yaml.  
init       Initialize mason in the current directory.  
list       Lists installed bricks.  
login      Log into brickhub.dev.  
logout     Log out of brickhub.dev.  
make       Generate code using an existing brick template.  
new        Creates a new brick template.  
publish    Publish the current brick to brickhub.dev.  
remove     Removes a brick.  
unbundle   Generates a brick template from a bundle.  
update     Update mason.  
upgrade    Upgrade bricks to their latest versions.  

Run "mason help <command>" for more information about a command.
</pre>